import DashboardLayout from "@/pages/Layout/DashboardLayout.vue";

import Dashboard from "@/pages/Dashboard.vue";
import UserProfile from "@/pages/UserProfile.vue";
import TableList from "@/pages/TableList.vue";
import Typography from "@/pages/Typography.vue";
import Icons from "@/pages/Icons.vue";
import Maps from "@/pages/Maps.vue";
import Notifications from "@/pages/Notifications.vue";
import UpgradeToPRO from "@/pages/UpgradeToPRO.vue";
import Login from "@/pages/Login";
import Router from "vue-router";

import CourseTable from "@/components/Tables/CourseTable";
import StudentTable from "@/components/Tables/StudentTable";
import TeacherTable from "@/components/Tables/TeacherTable";
import StudentView from "@/pages/StudentView";
import TeacherView from "@/pages/TeacherView";
import ClassTable from "@/components/Tables/ClassTable";
import ClassView from "@/pages/ClassView";
import CourseView from "@/pages/CourseView";
import CompetenceTable from "@/components/Tables/CompetenceTable";
import CompetenceView from "@/pages/CompetenceView";
import store from "@/store";

let router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      component: DashboardLayout,
      redirect: "/dashboard",
      children: [
        {
          path: "dashboard",
          name: "Dashboard",
          component: Dashboard,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "user",
          name: "User Profile",
          component: UserProfile,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "table",
          name: "Table List",
          component: TableList,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "students",
          name: "Students",
          component: StudentTable,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "students/:id",
          name: "Edit Student",
          component: StudentView,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "classes",
          name: "Classes",
          component: ClassTable,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "classes/:id",
          name: "Edit Class",
          component: ClassView,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "courses",
          name: "Courses",
          component: CourseTable,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "courses/:id",
          name: "Edit Course",
          component: CourseView,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "competences",
          name: "Competences",
          component: CompetenceTable,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "competences/:id",
          name: "Edit Competence",
          component: CompetenceView,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "teachers",
          name: "Teachers",
          component: TeacherTable,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "teachers/:id",
          name: "Edit Teacher",
          component: TeacherView,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "typography",
          name: "Typography",
          component: Typography,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "icons",
          name: "Icons",
          component: Icons,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "maps",
          name: "Maps",
          meta: {
            requiresAuth: true,
            hideFooter: true
          },
          component: Maps
        },
        {
          path: "notifications",
          name: "Notifications",
          component: Notifications
        },
        {
          path: "upgrade",
          name: "Upgrade to PRO",
          component: UpgradeToPRO
        }
      ]
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    }
  ],
  linkExactActiveClass: "nav-item active"
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next();
      return;
    }
    next("/login");
  } else {
    next();
  }
});

export default router;
