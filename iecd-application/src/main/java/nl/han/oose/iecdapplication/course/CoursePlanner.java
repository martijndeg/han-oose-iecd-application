package nl.han.oose.iecdapplication.course;

import nl.han.oose.iecdapplication.models.Course;
import nl.han.oose.iecdapplication.models.SchoolYear;

import java.time.LocalDate;

public class CoursePlanner {

    private Course course;
    private SchoolYear schoolYear;

    public CoursePlanner(Course course, SchoolYear schoolYear) {
        this.course = course;
        this.schoolYear = schoolYear;
    }

    public void plan() {

        for (LocalDate date = schoolYear.getStartDate(); date.isBefore(schoolYear.getEndDate()); date.plusDays(1)) {

        }
    }

}
