package nl.han.oose.iecdapplication.enums;

public enum EnrollmentStatus {

    ACTIVE,
    INACTIVE
}
