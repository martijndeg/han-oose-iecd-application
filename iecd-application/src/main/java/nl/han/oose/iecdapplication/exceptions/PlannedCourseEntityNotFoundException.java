package nl.han.oose.iecdapplication.exceptions;

public class PlannedCourseEntityNotFoundException extends RuntimeException {

    public PlannedCourseEntityNotFoundException(Long id) { super("Could not find Planned Course: " + id); }
}
