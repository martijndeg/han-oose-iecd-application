package nl.han.oose.iecdapplication.controllers;

import nl.han.oose.iecdapplication.entityModels.SchoolYearEntityModelAssembler;
import nl.han.oose.iecdapplication.enums.Status;
import nl.han.oose.iecdapplication.exceptions.SchoolYearEntityNotFoundException;
import nl.han.oose.iecdapplication.models.SchoolYear;
import nl.han.oose.iecdapplication.repositories.SchoolYearRepository;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.mediatype.vnderrors.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin(origins = "*")
@RestController
public class SchoolYearController {

    private SchoolYearRepository schoolYearRepository;
    private SchoolYearEntityModelAssembler assembler;

    public SchoolYearController(SchoolYearRepository schoolYearRepository, SchoolYearEntityModelAssembler assembler) {

        this.schoolYearRepository = schoolYearRepository;
        this.assembler = assembler;
    }

    @GetMapping("/schoolyears")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public CollectionModel<EntityModel<SchoolYear>> all() {

        List<EntityModel<SchoolYear>> schoolYears = schoolYearRepository.findAll().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());

        return new CollectionModel<>(schoolYears,
                linkTo(methodOn(SchoolYearController.class).all()).withSelfRel());
    }

    @PostMapping("/schoolYears")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<EntityModel<SchoolYear>> newSchoolYear(@RequestBody SchoolYear schoolYear) {

        SchoolYear newSchoolYear = schoolYearRepository.save(schoolYear);

        return ResponseEntity
                .created(linkTo(methodOn(SchoolYearController.class).one(newSchoolYear.getId())).toUri())
                .body(assembler.toModel(newSchoolYear));
    }

    @GetMapping("/schoolyears/{id}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public EntityModel<SchoolYear> one(@PathVariable Long id) {
        return assembler.toModel(schoolYearRepository.findById(id).orElseThrow(() -> new SchoolYearEntityNotFoundException(id)));
    }

    @PatchMapping("/schoolyears/{id}/activate")
    public ResponseEntity<RepresentationModel> activate(@PathVariable Long id) {

        SchoolYear schoolYear = schoolYearRepository.findById(id).orElseThrow(() -> new SchoolYearEntityNotFoundException(id));

        if(schoolYear.getStatus().equals(Status.IN_ACTIVE)) {
            schoolYear.setStatus(Status.ACTIVE);
            return ResponseEntity.ok(assembler.toModel(schoolYearRepository.save(schoolYear)));
        }

        return ResponseEntity
                .status(HttpStatus.METHOD_NOT_ALLOWED)
                .body(new VndErrors.VndError("Method not allowed", "You can't activate a school year that is in the " + schoolYear.getStatus() + " status"));
    }

    @DeleteMapping("/schoolyears/{id}/deactivate")
    public ResponseEntity<RepresentationModel> deactivate(@PathVariable Long id) {

        SchoolYear schoolYear = schoolYearRepository.findById(id).orElseThrow(() -> new SchoolYearEntityNotFoundException(id));

        if(schoolYear.getStatus().equals(Status.ACTIVE)) {
            schoolYear.setStatus(Status.IN_ACTIVE);
            return ResponseEntity.ok(assembler.toModel(schoolYearRepository.save(schoolYear)));
        }

        return ResponseEntity
                .status(HttpStatus.METHOD_NOT_ALLOWED)
                .body(new VndErrors.VndError("Method not allowed", "You can't deactivate a school year that is in the " + schoolYear.getStatus() + " status"));
    }

}
