package nl.han.oose.iecdapplication.models;

import lombok.Data;
import nl.han.oose.iecdapplication.enums.Status;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
public class Course {

    private @Id
    @GeneratedValue
    Long id;

    private String name;
    private String description;
    private Status status;

    public Course() {
    }

    public Course(String name, String description, Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    Set<Competence> competences = new HashSet<>();

    @ManyToOne
    Teacher createdBy;

    @ManyToMany(fetch = FetchType.EAGER)
    Set<Class> classes = new HashSet<>();
}
