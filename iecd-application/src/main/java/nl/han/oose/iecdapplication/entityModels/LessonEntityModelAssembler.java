package nl.han.oose.iecdapplication.entityModels;

import nl.han.oose.iecdapplication.models.Lesson;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class LessonEntityModelAssembler implements RepresentationModelAssembler<Lesson, EntityModel<Lesson>> {

    @Override
    public EntityModel<Lesson> toModel(Lesson lesson) {

        EntityModel<Lesson> lessonEntityModel = new EntityModel<>(lesson);
        return lessonEntityModel;
    }
}
