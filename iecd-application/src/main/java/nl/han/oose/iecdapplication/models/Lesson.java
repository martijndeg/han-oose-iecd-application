package nl.han.oose.iecdapplication.models;

import lombok.Data;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

@Data
@Entity
public class Lesson {

    @Id
    @GeneratedValue
    Long id;
    private DayOfWeek dayOfWeek;

    public Lesson() {

    }

    public Lesson(DayOfWeek dayOfWeek) {

        this.dayOfWeek = dayOfWeek;
    }

    @ManyToOne(cascade = {CascadeType.ALL})
    Class forClass;

    private LocalDate getDate(int year, int weekNumber) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.WEEK_OF_YEAR, weekNumber);
        calendar.set(Calendar.DAY_OF_WEEK, this.getDayOfWeek().getValue());

        return  LocalDate.parse(calendar.getTime().toString());
    }

    public Boolean isPlannable(int year, int weekNumber) {
        Vacation vacationInstance = new Vacation();
        LocalDate date = getDate(year, weekNumber);

        return !vacationInstance.isVacationDay(date);
    }
}
