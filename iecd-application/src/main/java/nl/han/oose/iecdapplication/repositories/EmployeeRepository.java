package nl.han.oose.iecdapplication.repositories;

import nl.han.oose.iecdapplication.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
