package nl.han.oose.iecdapplication.exceptions;

public class ClassEntityNotFoundException extends RuntimeException {

    public ClassEntityNotFoundException(Long id) { super("Could not find class " + id); }
}
