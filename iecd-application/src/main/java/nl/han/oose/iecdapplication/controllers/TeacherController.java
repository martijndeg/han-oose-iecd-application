package nl.han.oose.iecdapplication.controllers;

import nl.han.oose.iecdapplication.enums.EmploymentStatus;
import nl.han.oose.iecdapplication.exceptions.TeacherNotFoundException;
import nl.han.oose.iecdapplication.models.Teacher;
import nl.han.oose.iecdapplication.repositories.TeacherRepository;
import nl.han.oose.iecdapplication.entityModels.TeacherEntityModelAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.mediatype.vnderrors.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin(origins = "*")
@RestController
public class TeacherController {

    private final TeacherRepository teacherRepository;
    private final TeacherEntityModelAssembler assembler;

    public TeacherController(TeacherRepository teacherRepository, TeacherEntityModelAssembler assembler) {

        this.teacherRepository = teacherRepository;
        this.assembler = assembler;
    }

    @GetMapping("/teachers")
    public CollectionModel<EntityModel<Teacher>> all() {


        List<EntityModel<Teacher>> teachers = teacherRepository.findAll().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());

        return new CollectionModel<>(teachers,
                linkTo(methodOn(TeacherController.class).all()).withSelfRel());
    }

    @GetMapping("/teachers/{id}")
    public EntityModel<Teacher> one(@PathVariable Long id) {
        return assembler.toModel(teacherRepository.findById(id).orElseThrow(() -> new TeacherNotFoundException(id)));
    }

    @PostMapping("/teachers")
    public ResponseEntity<EntityModel<Teacher>> newTeacher(@RequestBody Teacher teacher) {

        Teacher newTeacher = teacherRepository.save(teacher);

        return ResponseEntity
                .created(linkTo(methodOn(TeacherController.class).one(newTeacher.getId())).toUri())
                .body(assembler.toModel(newTeacher));
    }

    @PutMapping("/teachers/{id}/activate")
    public ResponseEntity<RepresentationModel> activate(@PathVariable Long id) {

        Teacher teacher = teacherRepository.findById(id).orElseThrow(() -> new TeacherNotFoundException(id));

        if (teacher.getStatus().equals(EmploymentStatus.INACTIVE)) {
            teacher.setStatus(EmploymentStatus.ACTIVE);
            return ResponseEntity.ok(assembler.toModel(teacherRepository.save(teacher)));
        }

        return ResponseEntity
                .status(HttpStatus.METHOD_NOT_ALLOWED)
                .body(new VndErrors.VndError("Method not allowed", "You can't activate a teacher that is in the " + teacher.getStatus() + " status"));
    }

    @DeleteMapping("/teachers/{id}/deactivate")
    public ResponseEntity<RepresentationModel> deactivate(@PathVariable Long id) {

        Teacher teacher = teacherRepository.findById(id).orElseThrow(() -> new TeacherNotFoundException(id));

        if (teacher.getStatus().equals(EmploymentStatus.ACTIVE)) {
            teacher.setStatus(EmploymentStatus.INACTIVE);
            return ResponseEntity.ok(assembler.toModel(teacherRepository.save(teacher)));
        }

        return ResponseEntity
                .status(HttpStatus.METHOD_NOT_ALLOWED)
                .body(new VndErrors.VndError("Method not allowed", "You can't deactivate a teacher that is in the " + teacher.getStatus() + " status"));
    }
}
