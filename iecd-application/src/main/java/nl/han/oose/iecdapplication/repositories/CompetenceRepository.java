package nl.han.oose.iecdapplication.repositories;

import nl.han.oose.iecdapplication.models.Competence;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompetenceRepository extends JpaRepository<Competence, Long> {
}
