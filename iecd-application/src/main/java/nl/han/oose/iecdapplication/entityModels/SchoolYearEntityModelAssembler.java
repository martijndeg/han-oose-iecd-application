package nl.han.oose.iecdapplication.entityModels;

import nl.han.oose.iecdapplication.controllers.SchoolYearController;
import nl.han.oose.iecdapplication.controllers.TeacherController;
import nl.han.oose.iecdapplication.enums.EmploymentStatus;
import nl.han.oose.iecdapplication.enums.Status;
import nl.han.oose.iecdapplication.models.SchoolYear;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class SchoolYearEntityModelAssembler implements RepresentationModelAssembler<SchoolYear, EntityModel<SchoolYear>> {

    @Override
    public EntityModel<SchoolYear> toModel(SchoolYear schoolYear) {

        EntityModel<SchoolYear> schoolYearEntityModel = new EntityModel<>(schoolYear,
                linkTo(methodOn(SchoolYearController.class).one(schoolYear.getId())).withSelfRel(),
                linkTo(methodOn(SchoolYearController.class).all()).withSelfRel());

        if (schoolYear.getStatus().equals(Status.ACTIVE)) {
            schoolYearEntityModel.add(
                    linkTo(methodOn(SchoolYearController.class).deactivate(schoolYear.getId())).withRel("deactivate"));
        } else {
            schoolYearEntityModel.add(
                    linkTo(methodOn(SchoolYearController.class).activate(schoolYear.getId())).withRel("activate"));
        }


        return schoolYearEntityModel;
    }
}
