package nl.han.oose.iecdapplication.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import nl.han.oose.iecdapplication.enums.EmploymentStatus;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
public class Teacher extends Person {

    private EmploymentStatus status;
    @JsonIgnore
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @ManyToMany
    private Set<Role> roles = new HashSet<>();

    public Teacher() {}

    public Teacher(String firstName, String lastName, String email, String password, EmploymentStatus status) {
        super(firstName, lastName, email);
        this.password = password;
        this.status = status;
    }

    public EmploymentStatus getStatus() { return status; }

    public void setStatus(EmploymentStatus status) { this.status = status; }
}
