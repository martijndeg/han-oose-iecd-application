package nl.han.oose.iecdapplication.models;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
public class Rubric {

    private @Id
    @GeneratedValue
    Long id;
    private String name;

    public Rubric() {

    }

    public Rubric(String name) {
        this.name = name;
    }

    @ManyToOne
    Goal goal;

    @OneToMany(fetch = FetchType.EAGER)
    @OrderBy("result ASC")
    Set<RubricMetric> rubricMetrics = new HashSet<>();

    @ManyToOne
    Teacher createdBy;
}
