package nl.han.oose.iecdapplication.controllers;

import nl.han.oose.iecdapplication.enums.EmploymentStatus;
import nl.han.oose.iecdapplication.enums.UserRole;
import nl.han.oose.iecdapplication.models.Role;
import nl.han.oose.iecdapplication.models.Teacher;
import nl.han.oose.iecdapplication.repositories.RoleRepository;
import nl.han.oose.iecdapplication.repositories.TeacherRepository;
import nl.han.oose.iecdapplication.security.jwt.JwtUtils;
import nl.han.oose.iecdapplication.security.payload.request.LoginRequest;
import nl.han.oose.iecdapplication.security.payload.request.SignupRequest;
import nl.han.oose.iecdapplication.security.payload.response.JwtResponse;
import nl.han.oose.iecdapplication.security.payload.response.MessageResponse;
import nl.han.oose.iecdapplication.security.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                roles));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

//        if (teacherRepository.existsByEmail(signUpRequest.getEmail())) {
//            return ResponseEntity
//                    .badRequest()
//                    .body(new MessageResponse("Error: Email is already taken!"));
//        }

        // Create new teacher's account
        Teacher teacher = new Teacher(signUpRequest.getFirstName(), signUpRequest.getLastName(), signUpRequest.getEmail(),
                passwordEncoder.encode(signUpRequest.getPassword()), EmploymentStatus.valueOf(signUpRequest.getStatus()));

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(UserRole.USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(UserRole.ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    default:
                        Role userRole = roleRepository.findByName(UserRole.USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }

        teacher.setRoles(roles);
        teacherRepository.save(teacher);

        return ResponseEntity.ok(new MessageResponse("Teacher registered successfully!"));
    }
}
