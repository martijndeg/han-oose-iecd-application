package nl.han.oose.iecdapplication.repositories;

import nl.han.oose.iecdapplication.models.Rubric;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RubricRepository extends JpaRepository<Rubric, Long> {
}
