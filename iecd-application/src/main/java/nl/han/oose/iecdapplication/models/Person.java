package nl.han.oose.iecdapplication.models;

import lombok.Data;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;

@Data
@Entity
@Inheritance
@NoRepositoryBean
public abstract class Person {

    protected  @Id @GeneratedValue Long id;
    protected String firstName;
    protected String lastName;
    protected String email;

    public Person() {}

    public Person(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getName() { return firstName + ' ' +lastName; }
}