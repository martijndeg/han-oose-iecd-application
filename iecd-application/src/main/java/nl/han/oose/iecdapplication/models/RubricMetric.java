package nl.han.oose.iecdapplication.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Data
@Entity
public class RubricMetric {

    private @Id
    @GeneratedValue
    Long id;
    private String name;
    private String description;
    private int result;

    public RubricMetric() {

    }

    public RubricMetric(String name, String description, int result) {
        this.name = name;
        this.description = description;
        this.result = result;
    }

    @ManyToOne
    Rubric rubric;

    @ManyToOne
    Teacher createdBy;
}
