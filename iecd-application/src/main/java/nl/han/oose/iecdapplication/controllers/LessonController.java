package nl.han.oose.iecdapplication.controllers;

import nl.han.oose.iecdapplication.entityModels.LessonEntityModelAssembler;
import nl.han.oose.iecdapplication.exceptions.LessonEntityNotFoundException;
import nl.han.oose.iecdapplication.models.Lesson;
import nl.han.oose.iecdapplication.repositories.LessonRepository;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin(origins = "*")
@RestController
public class LessonController {
    
    private LessonRepository lessonRepository;
    private LessonEntityModelAssembler assembler;
    
    public LessonController(LessonRepository lessonRepository, LessonEntityModelAssembler assembler) {
        
        this.lessonRepository = lessonRepository;
        this.assembler = assembler;
    }

    @GetMapping("/lessons")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public CollectionModel<EntityModel<Lesson>> all() {

        List<EntityModel<Lesson>> lessons = lessonRepository.findAll().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());

        return new CollectionModel<>(lessons,
                linkTo(methodOn(LessonController.class).all()).withSelfRel());
    }

    @PostMapping("/lessons")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<EntityModel<Lesson>> newLesson(@RequestBody Lesson lesson) {

        Lesson newLesson = lessonRepository.save(lesson);

        return ResponseEntity
                .created(linkTo(methodOn(LessonController.class).one(newLesson.getId())).toUri())
                .body(assembler.toModel(newLesson));
    }

    @GetMapping("/lessons/{id}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public EntityModel<Lesson> one(@PathVariable Long id) {
        return assembler.toModel(lessonRepository.findById(id).orElseThrow(() -> new LessonEntityNotFoundException(id)));
    }
}
