package nl.han.oose.iecdapplication;

import lombok.extern.slf4j.Slf4j;

import nl.han.oose.iecdapplication.enums.EmploymentStatus;
import nl.han.oose.iecdapplication.enums.EnrollmentStatus;
import nl.han.oose.iecdapplication.enums.Status;
import nl.han.oose.iecdapplication.enums.UserRole;
import nl.han.oose.iecdapplication.models.*;
import nl.han.oose.iecdapplication.models.Class;
import nl.han.oose.iecdapplication.repositories.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Set;

@Configuration
@Slf4j
public class LoadDataBase {

//    private PasswordEncoder passwordEncoder;

    @Bean
    public CommandLineRunner initDataBase(LessonRepository lessonRepository, SchoolYearRepository schoolYearRepository, VacationRepository vacationRepository, CourseRepository courseRepository, StudentRepository studentRepository, TeacherRepository teacherRepository, ClassRepository classRepository, RoleRepository roleRepository, CompetenceRepository competenceRepository, GoalRepository goalRepository, RubricRepository rubricRepository, RubricMetricRepository rubricMetricRepository) {
        return args -> {
            Course oose = new Course("OOSE", "This is a course, which can be edited and used.", Status.ACTIVE);
            Course abd = new Course("ADB", "This is another course, which also can be edited and used.", Status.ACTIVE);

            SchoolYear schoolYear2020 = schoolYearRepository.save(new SchoolYear("2020-2021", LocalDate.of(2020, 8, 24), LocalDate.of(2021, 6, 26), Status.ACTIVE));

            Lesson tuesday = lessonRepository.save(new Lesson(DayOfWeek.TUESDAY));
            Lesson thursday = lessonRepository.save(new Lesson(DayOfWeek.THURSDAY));

            Vacation fallVacation = vacationRepository.save(new Vacation("Fall Holiday", LocalDate.of(2020, 10, 17), LocalDate.of(2020, 10, 25)));
            Vacation winterVacation = vacationRepository.save(new Vacation("Winter Holiday", LocalDate.of(2020, 12, 19), LocalDate.of(2021, 1, 3)));
            Vacation springVacation = vacationRepository.save(new Vacation("Spring Holiday", LocalDate.of(2021, 2, 13), LocalDate.of(2021, 2, 21)));
            Vacation mayVacation = vacationRepository.save(new Vacation("May Holiday", LocalDate.of(2021, 5, 1), LocalDate.of(2021, 5, 9)));
            Vacation summerVacation = vacationRepository.save(new Vacation("Summer Holiday", LocalDate.of(2021, 7, 24), LocalDate.of(2021, 9, 5)));

            schoolYear2020.getVacations().addAll(Arrays.asList(fallVacation, winterVacation, springVacation, mayVacation, summerVacation));

            Boolean isVacationDayOne = vacationRepository.isWithinOrOnVacation(LocalDate.of(2020, 12, 17));
            Boolean isVacationDayTwo = vacationRepository.isWithinOrOnVacation(LocalDate.of(2020, 12, 18));
            Boolean isVacationDayThree = vacationRepository.isWithinOrOnVacation(LocalDate.of(2020, 12, 19));
            Boolean isVacationDayFour = vacationRepository.isWithinOrOnVacation(LocalDate.of(2020, 12, 20));
            Boolean isVacationDayFive = vacationRepository.isWithinOrOnVacation(LocalDate.of(2021, 6, 3));

            System.out.println("Is this a vacation day (17-12-2020)? " + isVacationDayOne.toString());
            System.out.println("Is this a vacation day (18-12-2020)? " + isVacationDayTwo.toString());
            System.out.println("Is this a vacation day (19-12-2020)? " + isVacationDayThree.toString());
            System.out.println("Is this a vacation day (20-12-2020)? " + isVacationDayFour.toString());
            System.out.println("Is this a vacation day (03-06-2021)? " + isVacationDayFive.toString());


            schoolYearRepository.save(schoolYear2020);

            Role admin = new Role(UserRole.ADMIN);
            Role user = new Role(UserRole.USER);

            roleRepository.save(admin);
            roleRepository.save(user);

            Teacher hank = teacherRepository.save(new Teacher("Hank", "Schraeder", "hank@example.com", "$2y$12$tBUrgJ6VWtWgKhvSN1kP7uW3jQX/EIJ3Bl82suw5qhJEum83EW2rq", EmploymentStatus.ACTIVE));
            Teacher jimmy = teacherRepository.save(new Teacher("Jimmy", "McGill", "jimmy@example.com", "$2y$12$tBUrgJ6VWtWgKhvSN1kP7uW3jQX/EIJ3Bl82suw5qhJEum83EW2rq", EmploymentStatus.ACTIVE));

            jimmy.setRoles(Collections.singleton(admin));
            hank.setRoles(Collections.singleton(user));

            oose.setCreatedBy(hank);
            abd.setCreatedBy(jimmy);

            Student john = new Student("John", "Doe", "john@example.com", EnrollmentStatus.ACTIVE);
            Student jane = new Student("Jane", "Doe", "jane@example.com", EnrollmentStatus.ACTIVE);

            Competence competenceA = competenceRepository.save(new Competence("Applying Clean Code Rules", "Finding Code smells and have the ability to improve on found smells.", 4));
            Competence competenceB = competenceRepository.save(new Competence("Understanding and Applying Design Patterns", "Identifying, Understanding and Applying Design Patterns", 1));

            competenceA.setCreatedBy(hank);
            competenceB.setCreatedBy(hank);

            Class classA = classRepository.save(new Class("AA1C"));
            Class classB = classRepository.save(new Class("AB2B"));

            classA.setCreatedBy(hank);
            classB.setCreatedBy(hank);

            classA.getLessons().addAll(Arrays.asList(tuesday, thursday));

//            tuesday.setForClass(classA);
//            lessonRepository.save(tuesday);
//
//            thursday.setForClass(classA);
//            lessonRepository.save(thursday);

            classA.setCreatedBy(hank);
            classB.setCreatedBy(hank);

            classA.getStudents().addAll(Arrays.asList(john));
            classB.getStudents().addAll(Arrays.asList(jane));

            classA.getCourses().addAll(Arrays.asList(oose));
            classA.getCourses().addAll(Arrays.asList(abd));

            classB.getCourses().addAll(Arrays.asList(oose));

            oose.getCompetences().addAll(Arrays.asList(competenceA));
            oose.getCompetences().addAll(Arrays.asList(competenceB));

//            oose.setClasses(Collections.singleton(classA));

            Goal goalA = goalRepository.save(new Goal("Find Code Smells", "Finding Code Smells in all major categories", 25.0));
            Goal goalB = goalRepository.save(new Goal("Improve on Code Smells", "Improving on code with identified code smells", 25.0));

            goalA.setCreatedBy(hank);
            goalB.setCreatedBy(hank);

            Rubric rubricA = rubricRepository.save(new Rubric("Identifying Code Smells in own code"));
            Rubric rubricB = rubricRepository.save(new Rubric("Identifying Code Smells in provided code"));

            rubricA.setCreatedBy(hank);
            rubricB.setCreatedBy(hank);

            RubricMetric rubricAMetric40Perc = rubricMetricRepository.save(new RubricMetric("Some Smells found", "The student found some smells in the students' code, but fewer than half of the total.", 40));
            RubricMetric rubricAMetric60Perc = rubricMetricRepository.save(new RubricMetric("Most Smells found", "The student found most smells in the students' code, at least than half of the total.", 60));
            RubricMetric rubricAMetric80Perc = rubricMetricRepository.save(new RubricMetric("Almost all Smells found", "The student found almost all smells in the students' code, at least more than three quarters of the total.", 80));
            RubricMetric rubricAMetric100Perc = rubricMetricRepository.save(new RubricMetric("All Smells found", "The student found all smells in the students' code, all smells of the total.", 100));

            rubricAMetric40Perc.setCreatedBy(hank);
            rubricAMetric60Perc.setCreatedBy(hank);
            rubricAMetric80Perc.setCreatedBy(hank);
            rubricAMetric100Perc.setCreatedBy(hank);

            RubricMetric rubricBMetric40Perc = rubricMetricRepository.save(new RubricMetric("Some Smells found", "The student found some smells in the provided code, but fewer than half of the total.", 40));
            RubricMetric rubricBMetric60Perc = rubricMetricRepository.save(new RubricMetric("Most Smells found", "The student found most smells in the provided code, at least than half of the total.", 60));
            RubricMetric rubricBMetric80Perc = rubricMetricRepository.save(new RubricMetric("Almost all Smells found", "The student found almost all smells in the provided code, at least more than three quarters of the total.", 80));
            RubricMetric rubricBMetric100Perc = rubricMetricRepository.save(new RubricMetric("All Smells found", "The student found all smells in the provided code, all smells of the total.", 100));

            rubricBMetric40Perc.setCreatedBy(hank);
            rubricBMetric60Perc.setCreatedBy(hank);
            rubricBMetric80Perc.setCreatedBy(hank);
            rubricBMetric100Perc.setCreatedBy(hank);

            rubricA.getRubricMetrics().addAll(Arrays.asList(rubricAMetric40Perc, rubricAMetric60Perc, rubricAMetric80Perc, rubricAMetric100Perc));
            rubricB.getRubricMetrics().addAll(Arrays.asList(rubricBMetric40Perc, rubricBMetric60Perc, rubricBMetric80Perc, rubricBMetric100Perc));

            goalA.getRubrics().addAll(Arrays.asList(rubricA, rubricB));

            competenceA.getGoals().addAll(Arrays.asList(goalA, goalB));

            goalRepository.save(goalA);
            goalRepository.save(goalB);

            goalRepository.findAll().forEach(goal -> {
                log.info("Preloaded Goal " + goal.getName());
            });

            courseRepository.save(oose);
            courseRepository.save(abd);

            courseRepository.findAll().forEach(course -> {
                log.info("Preloaded Course " + course.getName());
            });

            rubricRepository.save(rubricA);
            rubricRepository.save(rubricB);

            rubricRepository.findAll().forEach(rubric -> {
                log.info("Preloaded Rubric " + rubric.getName());
            });

            rubricMetricRepository.findAll().forEach(rubricMetric -> {
                log.info("Preloaded Rubric Metric " + rubricMetric.getName() + " (" + rubricMetric.getDescription() + ")");
            });

            competenceRepository.save(competenceA);
            competenceRepository.save(competenceB);

            competenceRepository.findAll().forEach(competence -> {
                log.info("Preloaded Competence " + competence.getName());
            });

            studentRepository.save(john);
            studentRepository.save(jane);

            studentRepository.findAll().forEach(student -> {
                log.info("Preloaded Student " + student.getName());
            });

            classRepository.save(classA);
            classRepository.save(classB);

            classRepository.findAll().forEach(loadedClass -> {
                log.info("Preloaded Class " + loadedClass.getName());
            });

            teacherRepository.save(hank);

            teacherRepository.findAll().forEach(teacher -> {
                log.info("Preloaded Teacher " + teacher.getName());
            });

            lessonRepository.findAll().forEach(lesson -> {
                log.info("Preloaded Lesson " + lesson.getDayOfWeek());
            });

            schoolYearRepository.findAll().forEach(schoolYear -> {
                log.info("Preloaded SchoolYear " + schoolYear.getName());
            });

            vacationRepository.findAll().forEach(vacation -> {
                log.info("Preloaded Vacation " + vacation.getName());
            });

            log.info("Preloading database done!");
        };
    }
}
