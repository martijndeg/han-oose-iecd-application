package nl.han.oose.iecdapplication.controllers;

import nl.han.oose.iecdapplication.entityModels.RubricEntityModelAssembler;
import nl.han.oose.iecdapplication.exceptions.RubricEntityNotFoundException;
import nl.han.oose.iecdapplication.models.Rubric;
import nl.han.oose.iecdapplication.repositories.RubricRepository;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin(origins = "*")
@RestController
public class RubricController {

    private final RubricRepository rubricRepository;
    private final RubricEntityModelAssembler assembler;

    public RubricController(RubricRepository rubricRepository, RubricEntityModelAssembler assembler) {

        this.rubricRepository = rubricRepository;
        this.assembler = assembler;
    }

    @GetMapping("/rubrics")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public CollectionModel<EntityModel<Rubric>> all() {

        List<EntityModel<Rubric>> rubrics = rubricRepository.findAll().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());

        return new CollectionModel<>(rubrics,
                linkTo(methodOn(RubricController.class).all()).withSelfRel());
    }

    @PostMapping("/rubrics")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<EntityModel<Rubric>> newRubric(@RequestBody Rubric rubric) {

        Rubric newRubric = rubricRepository.save(rubric);

        return ResponseEntity
                .created(linkTo(methodOn(RubricController.class).one(newRubric.getId())).toUri())
                .body(assembler.toModel(newRubric));
    }

    @GetMapping("/rubrics/{id}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public EntityModel<Rubric> one(@PathVariable Long id) {
        return assembler.toModel(rubricRepository.findById(id).orElseThrow(() -> new RubricEntityNotFoundException(id)));
    }

}
