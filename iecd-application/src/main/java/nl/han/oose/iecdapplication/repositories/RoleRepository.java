package nl.han.oose.iecdapplication.repositories;

import nl.han.oose.iecdapplication.enums.UserRole;
import nl.han.oose.iecdapplication.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(UserRole role);
}
