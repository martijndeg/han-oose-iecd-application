package nl.han.oose.iecdapplication.models;

import lombok.Data;
import nl.han.oose.iecdapplication.enums.Status;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
public class SchoolYear {

    @Id
    @GeneratedValue
    Long id;

    private String name;
    private LocalDate startDate;
    private LocalDate endDate;
    private Status status;

    public SchoolYear() {

    }

    public SchoolYear(String name, LocalDate startDate, LocalDate endDate, Status status) {

        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
    }

    @OneToMany
    Set<Class> classes = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER)
    Set<Vacation> vacations = new HashSet<>();

    @ManyToOne
    Teacher createdBy;
}
