package nl.han.oose.iecdapplication.models;

import lombok.Data;
import nl.han.oose.iecdapplication.repositories.VacationRepository;
import org.hibernate.annotations.Table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@Entity
public class Vacation {

    private static VacationRepository vacationDayRepository;
    @Id
    @GeneratedValue
    Long id;

    private String name;
    private LocalDate startDate;
    private LocalDate endDate;

    public Vacation() {

    }

    public Vacation(String name, LocalDate startDate, LocalDate endDate) {

        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Vacation(String name, LocalDate startDate) {

        this.name = name;
        this.startDate = startDate;
    }

    public Boolean isVacationDay(LocalDate date) {
        return vacationDayRepository.isWithinOrOnVacation(date);
    }
}
