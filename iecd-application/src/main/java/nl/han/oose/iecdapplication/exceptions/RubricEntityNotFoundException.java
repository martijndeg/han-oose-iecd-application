package nl.han.oose.iecdapplication.exceptions;

public class RubricEntityNotFoundException extends RuntimeException {

    public RubricEntityNotFoundException(Long id) { super("Could not find Rubric: " + id); }
}
