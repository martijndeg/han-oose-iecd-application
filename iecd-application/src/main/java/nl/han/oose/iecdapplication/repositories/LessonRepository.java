package nl.han.oose.iecdapplication.repositories;

import nl.han.oose.iecdapplication.models.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LessonRepository extends JpaRepository<Lesson, Long> {
}
