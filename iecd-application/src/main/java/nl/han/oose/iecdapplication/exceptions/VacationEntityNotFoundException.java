package nl.han.oose.iecdapplication.exceptions;

public class VacationEntityNotFoundException extends RuntimeException {

    public  VacationEntityNotFoundException(Long id) { super("Vacation not found: " + id); }
}
