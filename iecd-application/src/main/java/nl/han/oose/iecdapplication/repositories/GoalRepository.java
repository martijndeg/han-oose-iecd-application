package nl.han.oose.iecdapplication.repositories;

import nl.han.oose.iecdapplication.models.Goal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoalRepository extends JpaRepository<Goal, Long> {
}
