package nl.han.oose.iecdapplication.controllers;

import nl.han.oose.iecdapplication.enums.Status;
import nl.han.oose.iecdapplication.exceptions.CourseNotFoundException;
import nl.han.oose.iecdapplication.models.Course;
import nl.han.oose.iecdapplication.repositories.CourseRepository;
import nl.han.oose.iecdapplication.entityModels.CourseEntityModelAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.mediatype.vnderrors.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin(origins = "*")
@RestController
public class CourseController {

    private final CourseRepository courseRepository;
    private final CourseEntityModelAssembler assembler;

    public CourseController(CourseRepository courseRepository, CourseEntityModelAssembler assembler) {

        this.courseRepository = courseRepository;
        this.assembler = assembler;
    }

    @GetMapping("/courses")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public CollectionModel<EntityModel<Course>> all() {

        List<EntityModel<Course>> courses = courseRepository.findAll().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());

        return new CollectionModel<>(courses,
                linkTo(methodOn(CourseController.class).all()).withSelfRel());
    }

    @GetMapping("/courses/{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public EntityModel<Course> one(@PathVariable Long id) {
        return assembler.toModel(courseRepository.findById(id).orElseThrow(() -> new CourseNotFoundException(id)));
    }

    @PostMapping("/courses")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<EntityModel<Course>> newCourse(@RequestBody Course course) {

        course.setStatus(Status.ACTIVE);
        Course newCourse = courseRepository.save(course);

        return ResponseEntity
                .created(linkTo(methodOn(CourseController.class).one(newCourse.getId())).toUri())
                .body(assembler.toModel(newCourse));
    }

    @PutMapping("/courses/{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<EntityModel<Course>> updateCourse(@RequestBody Course newCourse, @PathVariable Long id) {

        Course updatedCourse = courseRepository.findById(id)
                .map(course -> {
                    course.setName(newCourse.getName());
                    course.setDescription(newCourse.getDescription());

                    return courseRepository.save(course);
                })
                .orElseGet(() -> {
                    newCourse.setId(id);
                    return courseRepository.save(newCourse);
                });

        EntityModel<Course> resource = assembler.toModel(updatedCourse);

        return ResponseEntity
                .created(URI.create(resource.getLinks().toList().get(0).getHref()))
                .body(resource);
    }

    @DeleteMapping("/courses/{id}/deactivate")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<RepresentationModel> deactivate(@PathVariable Long id) {

        Course course = courseRepository.findById(id).orElseThrow(() -> new CourseNotFoundException(id));

        if (course.getStatus().equals(Status.ACTIVE)) {
            course.setStatus(Status.IN_ACTIVE);
            return ResponseEntity.ok(assembler.toModel(courseRepository.save(course)));
        }

        return ResponseEntity
                .status(HttpStatus.METHOD_NOT_ALLOWED)
                .body(new VndErrors.VndError("Method not allowed", "You can't deactivate an course that is in the " + course.getStatus() + " status"));
    }

    @PutMapping("/courses/{id}/activate")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<RepresentationModel> activate(@PathVariable Long id) {

        Course course = courseRepository.findById(id).orElseThrow(() -> new CourseNotFoundException(id));

        if (course.getStatus().equals(Status.IN_ACTIVE)) {
            course.setStatus(Status.ACTIVE);
            return ResponseEntity.ok(assembler.toModel(courseRepository.save(course)));
        }

        return ResponseEntity
                .status(HttpStatus.METHOD_NOT_ALLOWED)
                .body(new VndErrors.VndError("Method not allowed", "You can't activate an course that is in the " + course.getStatus() + " status"));
    }

}