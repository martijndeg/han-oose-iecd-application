package nl.han.oose.iecdapplication.repositories;

import nl.han.oose.iecdapplication.models.PlannedLesson;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlannedLessonRepository extends JpaRepository<PlannedLesson, Long> {

}
