package nl.han.oose.iecdapplication.entityModels;

import nl.han.oose.iecdapplication.controllers.StudentController;
import nl.han.oose.iecdapplication.enums.EnrollmentStatus;
import nl.han.oose.iecdapplication.models.Student;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class StudentEntityModelAssembler implements RepresentationModelAssembler<Student, EntityModel<Student>> {

    @Override
    public EntityModel<Student> toModel(Student student) {

        EntityModel<Student> studentEntityModel = new EntityModel<>(student,
                linkTo(methodOn(StudentController.class).one(student.getId())).withSelfRel(),
                linkTo(methodOn(StudentController.class).all()).withRel("students")
        );

        if (student.getStatus().equals(EnrollmentStatus.ACTIVE)) {
            studentEntityModel.add(
                    linkTo(methodOn(StudentController.class).deactivate(student.getId())).withRel("deactivate"));
        } else {
            studentEntityModel.add(
                    linkTo(methodOn(StudentController.class).activate(student.getId())).withRel("activate"));
        }

        return studentEntityModel;
    }
}
