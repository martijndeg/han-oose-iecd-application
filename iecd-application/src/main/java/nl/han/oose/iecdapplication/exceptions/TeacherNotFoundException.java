package nl.han.oose.iecdapplication.exceptions;

public class TeacherNotFoundException extends RuntimeException {
    public TeacherNotFoundException(Long id) { super("Could not find student " + id); }
}
