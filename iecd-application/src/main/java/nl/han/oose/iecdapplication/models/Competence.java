package nl.han.oose.iecdapplication.models;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
public class Competence {

    private @Id
    @GeneratedValue
    Long id;
    private String name;
    private String description;
    private int numberOfLessons;

    public Competence() {

    }

    public Competence(String name, String description, int numberOfLessons) {
        this.name = name;
        this.description = description;
        this.numberOfLessons = numberOfLessons;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    Set<Course> courses = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER)
    Set<Goal> goals = new HashSet<>();

    @ManyToOne
    Teacher createdBy;
}
