package nl.han.oose.iecdapplication.exceptions;

public class GoalEntityNotFoundException extends RuntimeException {

    public GoalEntityNotFoundException(Long id) { super("Could not find goal: " + id); }
}
