package nl.han.oose.iecdapplication.models;

import lombok.Data;
import nl.han.oose.iecdapplication.enums.UserRole;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class Role {

    @Id
    @GeneratedValue
    Long id;

    private UserRole name;

    public Role() {
    }

    public Role(UserRole name) {
        this.name = name;
    }
}
