package nl.han.oose.iecdapplication.exceptions;

public class LessonEntityNotFoundException extends RuntimeException {

    public LessonEntityNotFoundException(Long id) { super("Could not find Lesson: " + id); }
}
