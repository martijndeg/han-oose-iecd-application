package nl.han.oose.iecdapplication.controllers;

import nl.han.oose.iecdapplication.entityModels.GoalEntityModelAssembler;
import nl.han.oose.iecdapplication.exceptions.GoalEntityNotFoundException;
import nl.han.oose.iecdapplication.models.Goal;
import nl.han.oose.iecdapplication.repositories.GoalRepository;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin(origins = "*")
@RestController
public class GoalController {

    private final GoalRepository goalRepository;
    private final GoalEntityModelAssembler assembler;

    public GoalController(GoalRepository goalRepository, GoalEntityModelAssembler assembler) {

        this.goalRepository = goalRepository;
        this.assembler = assembler;
    }

    @GetMapping("/goals")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public CollectionModel<EntityModel<Goal>> all() {

        List<EntityModel<Goal>> goals = goalRepository.findAll().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());

        return new CollectionModel<>(goals,
                linkTo(methodOn(GoalController.class).all()).withSelfRel());
    }

    @PostMapping("/goals")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<EntityModel<Goal>> newGoal(@RequestBody Goal goal) {

        Goal newGoal = goalRepository.save(goal);

        return ResponseEntity
                .created(linkTo(methodOn(GoalController.class).one(newGoal.getId())).toUri())
                .body(assembler.toModel(newGoal));
    }

    @GetMapping("/goals/{id}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public EntityModel<Goal> one(@PathVariable Long id) {
        return assembler.toModel(goalRepository.findById(id).orElseThrow(() -> new GoalEntityNotFoundException(id)));
    }

}
