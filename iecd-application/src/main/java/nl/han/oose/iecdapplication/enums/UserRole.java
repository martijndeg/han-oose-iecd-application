package nl.han.oose.iecdapplication.enums;

public enum UserRole {
    ADMIN,
    USER
}
