package nl.han.oose.iecdapplication.enums;

public enum Status {

    ACTIVE,
    IN_ACTIVE,
}
