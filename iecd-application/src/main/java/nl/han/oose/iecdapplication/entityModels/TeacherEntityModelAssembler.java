package nl.han.oose.iecdapplication.entityModels;

import nl.han.oose.iecdapplication.controllers.TeacherController;
import nl.han.oose.iecdapplication.enums.EmploymentStatus;
import nl.han.oose.iecdapplication.models.Teacher;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class TeacherEntityModelAssembler implements RepresentationModelAssembler<Teacher, EntityModel<Teacher>> {

    @Override
    public EntityModel<Teacher> toModel(Teacher teacher) {

        EntityModel<Teacher> teacherEntityModel = new EntityModel<>(teacher,
                linkTo(methodOn(TeacherController.class).one(teacher.getId())).withSelfRel(),
                linkTo(methodOn(TeacherController.class).all()).withRel("teachers")
        );

        if (teacher.getStatus().equals(EmploymentStatus.ACTIVE)) {
            teacherEntityModel.add(
                    linkTo(methodOn(TeacherController.class).deactivate(teacher.getId())).withRel("deactivate"));
        } else {
            teacherEntityModel.add(
                    linkTo(methodOn(TeacherController.class).activate(teacher.getId())).withRel("activate"));
        }

        return teacherEntityModel;
    }
}
