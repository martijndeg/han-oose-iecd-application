package nl.han.oose.iecdapplication.entityModels;

import nl.han.oose.iecdapplication.models.PlannedLesson;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class PlannedLessonEntityModelAssembler implements RepresentationModelAssembler<PlannedLesson, EntityModel<PlannedLesson>> {

    @Override
    public EntityModel<PlannedLesson> toModel(PlannedLesson plannedLesson) {

        EntityModel<PlannedLesson> plannedLessonEntityModel = new EntityModel<>(plannedLesson);

        return plannedLessonEntityModel;
    }
}
