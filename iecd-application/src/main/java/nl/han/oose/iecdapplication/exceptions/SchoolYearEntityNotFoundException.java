package nl.han.oose.iecdapplication.exceptions;

public class SchoolYearEntityNotFoundException extends RuntimeException {

    public SchoolYearEntityNotFoundException(Long id) { super("Could not find Schoolyear: " + id); }
}
