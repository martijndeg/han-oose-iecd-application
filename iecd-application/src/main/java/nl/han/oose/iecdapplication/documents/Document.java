package nl.han.oose.iecdapplication.documents;

public interface Document {

    public void generate();
}
