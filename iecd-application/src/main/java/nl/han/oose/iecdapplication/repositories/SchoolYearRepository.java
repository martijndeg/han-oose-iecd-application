package nl.han.oose.iecdapplication.repositories;

import nl.han.oose.iecdapplication.models.SchoolYear;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SchoolYearRepository extends JpaRepository<SchoolYear, Long> {
}
