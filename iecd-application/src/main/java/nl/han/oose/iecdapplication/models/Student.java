package nl.han.oose.iecdapplication.models;

import lombok.Data;
import nl.han.oose.iecdapplication.enums.EnrollmentStatus;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.Set;

@Data
@Entity
public class Student extends Person {

    private EnrollmentStatus status;

    public Student() {}

    public Student(String firstName, String lastName, String email, EnrollmentStatus status) {

        super(firstName, lastName, email);
        this.status = status;
    }

    public EnrollmentStatus getStatus() {
        return status;
    }

    public void setStatus(EnrollmentStatus status) {
        this.status = status;
    }

    @ManyToMany
    Set<Class> classes;
}