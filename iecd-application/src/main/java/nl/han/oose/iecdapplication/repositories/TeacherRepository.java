package nl.han.oose.iecdapplication.repositories;

import nl.han.oose.iecdapplication.models.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long> {

    Teacher findByEmail(String email);

    Boolean existsByEmail(String email);
}
