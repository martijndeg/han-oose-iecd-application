package nl.han.oose.iecdapplication.repositories;

import nl.han.oose.iecdapplication.models.Class;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassRepository extends JpaRepository<Class, Long> {
}
