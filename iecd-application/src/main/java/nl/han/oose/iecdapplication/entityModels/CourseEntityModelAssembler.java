package nl.han.oose.iecdapplication.entityModels;

import nl.han.oose.iecdapplication.controllers.CourseController;
import nl.han.oose.iecdapplication.enums.Status;
import nl.han.oose.iecdapplication.models.Course;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class CourseEntityModelAssembler implements RepresentationModelAssembler<Course, EntityModel<Course>> {

    @Override
    public EntityModel<Course> toModel(Course course) {

        EntityModel<Course> courseEntityModel = new EntityModel<>(course,
                linkTo(methodOn(CourseController.class).one(course.getId())).withSelfRel(),
                linkTo(methodOn(CourseController.class).all()).withRel("courses")
        );

        if (course.getStatus().equals(Status.ACTIVE)) {
            courseEntityModel.add(
                    linkTo(methodOn(CourseController.class).deactivate(course.getId())).withRel("deactivate"));
            courseEntityModel.add(
                    linkTo(methodOn(CourseController.class).activate(course.getId())).withRel("activate"));
        }

        return courseEntityModel;
    }

}
