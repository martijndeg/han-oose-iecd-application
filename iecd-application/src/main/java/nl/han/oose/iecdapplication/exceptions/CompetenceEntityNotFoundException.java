package nl.han.oose.iecdapplication.exceptions;

public class CompetenceEntityNotFoundException extends RuntimeException {

    public CompetenceEntityNotFoundException(Long id) { super("Could not find competence: " + id); }
}
