package nl.han.oose.iecdapplication.controllers;


import nl.han.oose.iecdapplication.entityModels.PlannedCourseEntityModelAssembler;
import nl.han.oose.iecdapplication.enums.Status;
import nl.han.oose.iecdapplication.exceptions.PlannedCourseEntityNotFoundException;
import nl.han.oose.iecdapplication.models.PlannedCourse;
import nl.han.oose.iecdapplication.repositories.PlannedCourseRepository;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.mediatype.vnderrors.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin(origins = "*")
@RestController
public class PlannedCourseController {

    private final PlannedCourseRepository plannedCourseRepository;
    private final PlannedCourseEntityModelAssembler assembler;

    public PlannedCourseController(PlannedCourseRepository plannedCourseRepository, PlannedCourseEntityModelAssembler assembler) {
        this.plannedCourseRepository = plannedCourseRepository;
        this.assembler = assembler;
    }

    @GetMapping("/planned-courses")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public CollectionModel<EntityModel<PlannedCourse>> all() {

        List<EntityModel<PlannedCourse>> plannedCourses = plannedCourseRepository.findAll().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());

        return new CollectionModel<>(plannedCourses,
                linkTo(methodOn(PlannedCourseController.class).all()).withSelfRel());
    }

    @GetMapping("/planned-courses/{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public EntityModel<PlannedCourse> one(@PathVariable Long id) {
        return assembler.toModel(plannedCourseRepository.findById(id).orElseThrow(() -> new PlannedCourseEntityNotFoundException(id)));
    }

    @DeleteMapping("/planned-courses/{id}/deactivate")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<RepresentationModel> deactivate(@PathVariable Long id) {

        PlannedCourse plannedCourse = plannedCourseRepository.findById(id).orElseThrow(() -> new PlannedCourseEntityNotFoundException(id));

        if (plannedCourse.getStatus().equals(Status.ACTIVE)) {
            plannedCourse.setStatus(Status.IN_ACTIVE);
            return ResponseEntity.ok(assembler.toModel(plannedCourseRepository.save(plannedCourse)));
        }

        return ResponseEntity
                .status(HttpStatus.METHOD_NOT_ALLOWED)
                .body(new VndErrors.VndError("Method not allowed", "You can't deactivate an plannedCourse that is in the " + plannedCourse.getStatus() + " status"));
    }

    @PutMapping("/plannedCourses/{id}/activate")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<RepresentationModel> activate(@PathVariable Long id) {

        PlannedCourse plannedCourse = plannedCourseRepository.findById(id).orElseThrow(() -> new PlannedCourseEntityNotFoundException(id));

        if (plannedCourse.getStatus().equals(Status.IN_ACTIVE)) {
            plannedCourse.setStatus(Status.ACTIVE);
            return ResponseEntity.ok(assembler.toModel(plannedCourseRepository.save(plannedCourse)));
        }

        return ResponseEntity
                .status(HttpStatus.METHOD_NOT_ALLOWED)
                .body(new VndErrors.VndError("Method not allowed", "You can't activate an planned course that is in the " + plannedCourse.getStatus() + " status"));
    }


}
