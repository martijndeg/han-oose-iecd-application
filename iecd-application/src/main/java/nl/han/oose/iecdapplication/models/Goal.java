package nl.han.oose.iecdapplication.models;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
public class Goal {

    private @Id
    @GeneratedValue
    Long id;
    private String name;
    private String description;
    private int weight;

    public Goal() {

    }

    public Goal(String name, String description, double weight) {
        this.name = name;
        this.description = description;
        this.setWeight(weight);
    }

    public void setWeight(double value) {
        this.weight = (int) value * 100;
    }

    public double getWeight() {
        return ((double) this.weight) / 100;
    }

    @OneToMany(fetch = FetchType.EAGER)
    Set<Rubric> rubrics = new HashSet<>();

    @ManyToOne
    Teacher createdBy;
}
