package nl.han.oose.iecdapplication.exceptions;

public class PlannedLessonEntityNotFoundException extends RuntimeException {

    public PlannedLessonEntityNotFoundException(Long id) { super("Could not find Planned Lesson: " + id); }
}
