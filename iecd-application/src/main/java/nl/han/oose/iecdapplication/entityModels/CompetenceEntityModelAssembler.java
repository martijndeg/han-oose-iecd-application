package nl.han.oose.iecdapplication.entityModels;

import nl.han.oose.iecdapplication.controllers.CompetenceController;
import nl.han.oose.iecdapplication.models.Competence;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class CompetenceEntityModelAssembler implements RepresentationModelAssembler<Competence, EntityModel<Competence>> {

    @Override
    public EntityModel<Competence> toModel(Competence competenceEntity) {

        EntityModel<Competence> competenceEntityModel = new EntityModel<>(competenceEntity,
                linkTo(methodOn(CompetenceController.class).one(competenceEntity.getId())).withSelfRel(),
                linkTo(methodOn(CompetenceController.class).all()).withRel("competences")
        );
        return competenceEntityModel;
    }
}
