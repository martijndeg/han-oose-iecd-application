package nl.han.oose.iecdapplication.controllers;

import nl.han.oose.iecdapplication.entityModels.PlannedLessonEntityModelAssembler;
import nl.han.oose.iecdapplication.exceptions.PlannedLessonEntityNotFoundException;
import nl.han.oose.iecdapplication.models.PlannedLesson;
import nl.han.oose.iecdapplication.repositories.PlannedLessonRepository;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin(origins = "*")
@RestController
public class PlannedLessonController {

    private final PlannedLessonRepository plannedLessonRepository;
    private final PlannedLessonEntityModelAssembler assembler;

    public PlannedLessonController(PlannedLessonRepository plannedLessonRepository, PlannedLessonEntityModelAssembler assembler) {

        this.plannedLessonRepository = plannedLessonRepository;
        this.assembler = assembler;
    }

    @GetMapping("/planned-lessons")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public CollectionModel<EntityModel<PlannedLesson>> all() {

        List<EntityModel<PlannedLesson>> plannedLessons = plannedLessonRepository.findAll().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());

        return new CollectionModel<>(plannedLessons,
                linkTo(methodOn(PlannedLessonController.class).all()).withSelfRel());
    }

    @GetMapping("/planned-lessons/{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public EntityModel<PlannedLesson> one(@PathVariable Long id) {
        return assembler.toModel(plannedLessonRepository.findById(id).orElseThrow(() -> new PlannedLessonEntityNotFoundException(id)));
    }
}
