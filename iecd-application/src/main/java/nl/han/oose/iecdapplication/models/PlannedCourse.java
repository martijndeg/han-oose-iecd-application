package nl.han.oose.iecdapplication.models;

import lombok.Data;
import nl.han.oose.iecdapplication.enums.Status;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@Data
@Entity
public class PlannedCourse {

    private @Id
    @GeneratedValue
    Long id;

    private Status status;
    private LocalDate startDate;
    private LocalDate endDate;

    @ManyToOne
    private Teacher plannedBy;

    @ManyToOne
    private Course course;
}
