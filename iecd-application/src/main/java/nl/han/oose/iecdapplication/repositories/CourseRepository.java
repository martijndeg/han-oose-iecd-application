package nl.han.oose.iecdapplication.repositories;

import nl.han.oose.iecdapplication.models.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {
}
