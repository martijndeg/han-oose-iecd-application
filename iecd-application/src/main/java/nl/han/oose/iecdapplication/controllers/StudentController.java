package nl.han.oose.iecdapplication.controllers;

import nl.han.oose.iecdapplication.enums.EnrollmentStatus;
import nl.han.oose.iecdapplication.exceptions.StudentNotFoundException;
import nl.han.oose.iecdapplication.models.Student;
import nl.han.oose.iecdapplication.repositories.StudentRepository;
import nl.han.oose.iecdapplication.entityModels.StudentEntityModelAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.mediatype.vnderrors.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin(origins = "*")
@RestController
public class StudentController {

    private final StudentRepository studentRepository;
    private final StudentEntityModelAssembler assembler;

    public StudentController(StudentRepository studentRepository, StudentEntityModelAssembler assembler) {

        this.studentRepository = studentRepository;
        this.assembler = assembler;
    }

    @GetMapping("/students")
    public CollectionModel<EntityModel<Student>> all() {


        List<EntityModel<Student>> students = studentRepository.findAll().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());

        return new CollectionModel<>(students,
                linkTo(methodOn(StudentController.class).all()).withSelfRel());
    }

    @GetMapping("/students/{id}")
    public EntityModel<Student> one(@PathVariable Long id) {
        return assembler.toModel(studentRepository.findById(id).orElseThrow(() -> new StudentNotFoundException(id)));
    }

    @PostMapping("/students")
    public ResponseEntity<EntityModel<Student>> newStudent(@RequestBody Student student) {

        Student newStudent = studentRepository.save(student);

        return ResponseEntity
                .created(linkTo(methodOn(StudentController.class).one(newStudent.getId())).toUri())
                .body(assembler.toModel(newStudent));
    }

    @PutMapping("/students/{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<EntityModel<Student>> updateStudent(@RequestBody Student newStudent, @PathVariable Long id) {

        Student updatedStudent = studentRepository.findById(id)
                .map(student -> {
                    student.setFirstName(newStudent.getFirstName());
                    student.setLastName(newStudent.getLastName());
                    student.setEmail(newStudent.getEmail());

                    return studentRepository.save(student);
                })
                .orElseGet(() -> {
                    newStudent.setId(id);
                    return studentRepository.save(newStudent);
                });

        EntityModel<Student> resource = assembler.toModel(updatedStudent);

        return ResponseEntity
                .created(URI.create(resource.getLinks().toList().get(0).getHref()))
                .body(resource);
    }

    @PutMapping("/students/{id}/activate")
    public ResponseEntity<RepresentationModel> activate(@PathVariable Long id) {

        Student student = studentRepository.findById(id).orElseThrow(() -> new StudentNotFoundException(id));

        if (student.getStatus().equals(EnrollmentStatus.INACTIVE)) {
            student.setStatus(EnrollmentStatus.ACTIVE);
            return ResponseEntity.ok(assembler.toModel(studentRepository.save(student)));
        }

        return ResponseEntity
                .status(HttpStatus.METHOD_NOT_ALLOWED)
                .body(new VndErrors.VndError("Method not allowed", "You can't activate a student that is in the " + student.getStatus() + " status"));
    }

    @DeleteMapping("/students/{id}/deactivate")
    public ResponseEntity<RepresentationModel> deactivate(@PathVariable Long id) {

        Student student = studentRepository.findById(id).orElseThrow(() -> new StudentNotFoundException(id));

        if (student.getStatus().equals(EnrollmentStatus.ACTIVE)) {
            student.setStatus(EnrollmentStatus.INACTIVE);
            return ResponseEntity.ok(assembler.toModel(studentRepository.save(student)));
        }

        return ResponseEntity
                .status(HttpStatus.METHOD_NOT_ALLOWED)
                .body(new VndErrors.VndError("Method not allowed", "You can't deactivate a student that is in the " + student.getStatus() + " status"));
    }
}
