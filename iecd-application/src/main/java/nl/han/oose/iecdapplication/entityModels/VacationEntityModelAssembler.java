package nl.han.oose.iecdapplication.entityModels;

import nl.han.oose.iecdapplication.controllers.VacationController;
import nl.han.oose.iecdapplication.models.Vacation;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class VacationEntityModelAssembler implements RepresentationModelAssembler<Vacation, EntityModel<Vacation>> {

    @Override
    public EntityModel<Vacation> toModel(Vacation vacation) {

        EntityModel<Vacation> vacationEntityModel = new EntityModel<>(vacation,
                linkTo(methodOn(VacationController.class).one(vacation.getId())).withSelfRel(),
                linkTo(methodOn(VacationController.class).all()).withRel("vacations"));

        return vacationEntityModel;
    }
}
