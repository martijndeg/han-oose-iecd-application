package nl.han.oose.iecdapplication.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
public class PlannedLesson {

    private
    @GeneratedValue
    @Id
    Long id;

    @ManyToMany
    private Set<Goal> goals = new HashSet<>();
}
