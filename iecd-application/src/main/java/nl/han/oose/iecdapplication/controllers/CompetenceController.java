package nl.han.oose.iecdapplication.controllers;

import nl.han.oose.iecdapplication.exceptions.CompetenceEntityNotFoundException;
import nl.han.oose.iecdapplication.exceptions.TeacherNotFoundException;
import nl.han.oose.iecdapplication.models.Competence;
import nl.han.oose.iecdapplication.models.Teacher;
import nl.han.oose.iecdapplication.repositories.CompetenceRepository;
import nl.han.oose.iecdapplication.entityModels.CompetenceEntityModelAssembler;
import nl.han.oose.iecdapplication.repositories.TeacherRepository;
import nl.han.oose.iecdapplication.security.services.UserDetailsImpl;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin(origins = "*")
@RestController
public class CompetenceController {

    private final CompetenceRepository competenceRepository;
    private final CompetenceEntityModelAssembler assembler;

    private final TeacherRepository teacherRepository;

    public CompetenceController(CompetenceRepository competenceRepository, CompetenceEntityModelAssembler assembler, TeacherRepository teacherRepository) {
        this.competenceRepository = competenceRepository;
        this.assembler = assembler;

        this.teacherRepository = teacherRepository;
    }

    @GetMapping("/competences")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public CollectionModel<EntityModel<Competence>> all() {

        List<EntityModel<Competence>> competences = competenceRepository.findAll().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());

        return new CollectionModel<>(competences,
                linkTo(methodOn(CompetenceController.class).all()).withSelfRel());
    }

    @GetMapping("/competences/{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public EntityModel<Competence> one(@PathVariable Long id) {
        return assembler.toModel(competenceRepository.findById(id).orElseThrow(() -> new CompetenceEntityNotFoundException(id)));
    }

    @PostMapping("/competences")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<EntityModel<Competence>> newCompetence(@RequestBody Competence competence) {

        Competence newCompetence = competenceRepository.save(competence);

        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Optional<Teacher> creator = teacherRepository.findById(userDetails.getId());
        newCompetence.setCreatedBy(creator.get());

        competenceRepository.save(newCompetence);

        return ResponseEntity
                .created(linkTo(methodOn(CompetenceController.class).one(newCompetence.getId())).toUri())
                .body(assembler.toModel(newCompetence));
    }

    @PutMapping("/competences/{id}")
    public ResponseEntity<EntityModel<Competence>> replaceCompetence(@RequestBody Competence newCompetence, @PathVariable Long id) {

        Competence updatedCompetence = competenceRepository.findById(id)
                .map(competence -> {
                    competence.setName(newCompetence.getName());
                    competence.setDescription(newCompetence.getDescription());
                    return competenceRepository.save(competence);
                })
                .orElseGet(() -> {
                    newCompetence.setId(id);
                    return competenceRepository.save(newCompetence);
                });

        EntityModel<Competence> resource = assembler.toModel(updatedCompetence);

        return ResponseEntity
                .created(URI.create(resource.getLinks().toList().get(0).getHref()))
                .body(resource);
    }
}
