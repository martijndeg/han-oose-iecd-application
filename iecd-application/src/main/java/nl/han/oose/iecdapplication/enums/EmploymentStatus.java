package nl.han.oose.iecdapplication.enums;

public enum EmploymentStatus {

    ACTIVE,
    INACTIVE,
}
