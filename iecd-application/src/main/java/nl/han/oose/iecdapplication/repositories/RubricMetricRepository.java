package nl.han.oose.iecdapplication.repositories;

import nl.han.oose.iecdapplication.models.RubricMetric;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RubricMetricRepository extends JpaRepository<RubricMetric, Long> {
}
