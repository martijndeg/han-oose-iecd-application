package nl.han.oose.iecdapplication.repositories;

import nl.han.oose.iecdapplication.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {

}
