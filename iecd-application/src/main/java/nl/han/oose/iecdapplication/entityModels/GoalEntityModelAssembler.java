package nl.han.oose.iecdapplication.entityModels;

import nl.han.oose.iecdapplication.models.Goal;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class GoalEntityModelAssembler implements RepresentationModelAssembler<Goal, EntityModel<Goal>> {

    @Override
    public EntityModel<Goal> toModel(Goal goalEntity) {

        EntityModel<Goal> goalEntityModel = new EntityModel<>(goalEntity);
        return goalEntityModel;
    }
}
