package nl.han.oose.iecdapplication.repositories;

import nl.han.oose.iecdapplication.models.PlannedCourse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlannedCourseRepository extends JpaRepository<PlannedCourse, Long> {
}
