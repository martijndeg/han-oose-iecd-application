package nl.han.oose.iecdapplication.controllers;

import nl.han.oose.iecdapplication.exceptions.ClassEntityNotFoundException;

import nl.han.oose.iecdapplication.models.Class;
import nl.han.oose.iecdapplication.repositories.ClassRepository;
import nl.han.oose.iecdapplication.entityModels.ClassEntityModelAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin(origins = "*")
@RestController
public class ClassController {

    private final ClassRepository classRepository;
    private final ClassEntityModelAssembler assembler;

    public ClassController(ClassRepository classRepository, ClassEntityModelAssembler assembler) {

        this.classRepository = classRepository;
        this.assembler = assembler;
    }

    @GetMapping("/classes")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public CollectionModel<EntityModel<Class>> all() {

        List<EntityModel<Class>> classes = classRepository.findAll().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());

        return new CollectionModel<>(classes,
                linkTo(methodOn(ClassController.class).all()).withSelfRel());
    }

    @GetMapping("/classes/{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public EntityModel<Class> one(@PathVariable Long id) {
        return assembler.toModel(classRepository.findById(id).orElseThrow(() -> new ClassEntityNotFoundException(id)));
    }

    @PostMapping("/classes")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<EntityModel<Class>> newClass(@RequestBody Class classEntity) {

        Class newClass = classRepository.save(classEntity);

        return ResponseEntity
                .created(linkTo(methodOn(ClassController.class).one(newClass.getId())).toUri())
                .body(assembler.toModel(newClass));
    }

    @PutMapping("/classes/{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<EntityModel<Class>> updateClass(@RequestBody Class newClass, @PathVariable Long id) {

        Class updatedClass = classRepository.findById(id)
                .map(classObject -> {
                    classObject.setName(newClass.getName());
                    return classRepository.save(classObject);
                })
                .orElseGet(() -> {
                    newClass.setId(id);
                    return classRepository.save(newClass);
                });

        EntityModel<Class> resource = assembler.toModel(updatedClass);

        return ResponseEntity
                .created(URI.create(resource.getLinks().toList().get(0).getHref()))
                .body(resource);
    }

//    @DeleteMapping("/classes/{id}/cancel")
//    public ResponseEntity<RepresentationModel> cancel(@PathVariable Long id) {

//        Class class = classRepository.findById(id).orElseThrow(() -> new ClassNotFoundException(id));
//
//        if (class.getStatus().equals(Status.ACTIVE)) {
//            class.setStatus(Status.ACTIVE);
//            return ResponseEntity.ok(assembler.toModel(classRepository.save(class)));
//        }
//
//        return ResponseEntity
//                .status(HttpStatus.METHOD_NOT_ALLOWED)
//                .body(new VndErrors.VndError("Method not allowed", "You can't cancel an class that is in the " + class.getStatus() + " status"));
//    }

//    @PutMapping("/classes/{id}/complete")
//    public ResponseEntity<RepresentationModel> complete(@PathVariable Long id) {
//
//        Class classEntity = classRepository.findById(id).orElseThrow(() -> new ClassEntityNotFoundException(id));

//        if (class.getStatus().equals(Status.ACTIVE)) {
//            class.setStatus(Status.IN_ACTIVE);
//            return ResponseEntity.ok(assembler.toModel(classRepository.save(class)));
//        }

//        return ResponseEntity
//                .status(HttpStatus.METHOD_NOT_ALLOWED)
//                .body(new VndErrors.VndError("Method not allowed", "You can't complete an class that is in the " + class.getStatus() + " status"));
//    }

}
