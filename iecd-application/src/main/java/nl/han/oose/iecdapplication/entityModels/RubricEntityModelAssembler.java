package nl.han.oose.iecdapplication.entityModels;

import nl.han.oose.iecdapplication.models.Rubric;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class RubricEntityModelAssembler implements RepresentationModelAssembler<Rubric, EntityModel<Rubric>> {

    @Override
    public EntityModel<Rubric> toModel(Rubric rubric) {

        EntityModel<Rubric> rubricEntityModel = new EntityModel<>(rubric);
        return rubricEntityModel;
    }
}
