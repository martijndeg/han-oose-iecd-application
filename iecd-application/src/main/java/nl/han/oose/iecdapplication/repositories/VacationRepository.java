package nl.han.oose.iecdapplication.repositories;

import nl.han.oose.iecdapplication.models.Vacation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;

public interface VacationRepository extends JpaRepository<Vacation, Long> {

    @Query(value = "SELECT CASE WHEN COUNT(v) > 0  THEN true ELSE false END from Vacation v WHERE (:date BETWEEN v.startDate AND v.endDate) OR (:date = v.startDate)")
    public Boolean isWithinOrOnVacation(@Param("date") LocalDate date);
//    Boolean existsByStartDateBeforeOrStartDateEqualsOrStartDateBeforeOrStartDateEqualsAndEndDateAfterOrEndDateEquals(LocalDate date);
}
