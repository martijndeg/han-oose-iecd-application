package nl.han.oose.iecdapplication.entityModels;

import nl.han.oose.iecdapplication.controllers.ClassController;
import nl.han.oose.iecdapplication.models.Class;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class ClassEntityModelAssembler implements RepresentationModelAssembler<Class, EntityModel<Class>> {

    @Override
    public EntityModel<Class> toModel(Class classEntity) {

        EntityModel<Class> classEntityModel = new EntityModel<>(classEntity,
                linkTo(methodOn(ClassController.class).one(classEntity.getId())).withSelfRel(),
                linkTo(methodOn(ClassController.class).all()).withRel("classes")
        );

//        if (classEntity.getStatus().equals(Status.ACTIVE)) {
//            classEntityModel.add(
//                    linkTo(methodOn(ClassController.class).cancel(classEntity.getId())).withRel("cancel"));
//            classEntityModel.add(
//                    linkTo(methodOn(ClassController.class).complete(classEntity.getId())).withRel("complete"));
//        }

        return classEntityModel;
    }
}
