package nl.han.oose.iecdapplication.controllers;

import nl.han.oose.iecdapplication.exceptions.VacationEntityNotFoundException;
import nl.han.oose.iecdapplication.models.Vacation;
import nl.han.oose.iecdapplication.repositories.VacationRepository;
import nl.han.oose.iecdapplication.entityModels.VacationEntityModelAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin(origins = "*")
@RestController
public class VacationController {

    private VacationRepository vacationRepository;
    private VacationEntityModelAssembler assembler;

    public VacationController(VacationRepository vacationRepository, VacationEntityModelAssembler assembler) {

        this.vacationRepository = vacationRepository;
        this.assembler = assembler;
    }

    @GetMapping("/vacations")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public CollectionModel<EntityModel<Vacation>> all() {

        List<EntityModel<Vacation>> vacations = vacationRepository.findAll().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());

        return new CollectionModel<>(vacations,
                linkTo(methodOn(VacationController.class).all()).withSelfRel());
    }

    @PostMapping("/vacations")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<EntityModel<Vacation>> newVacation(@RequestBody Vacation vacation) {

        Vacation newVacation = vacationRepository.save(vacation);

        return ResponseEntity
                .created(linkTo(methodOn(VacationController.class).one(newVacation.getId())).toUri())
                .body(assembler.toModel(newVacation));
    }

    @GetMapping("/vacations/{id}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public EntityModel<Vacation> one(@PathVariable Long id) {
        return assembler.toModel(vacationRepository.findById(id).orElseThrow(() -> new VacationEntityNotFoundException(id)));
    }
}
