package nl.han.oose.iecdapplication.entityModels;

import nl.han.oose.iecdapplication.controllers.PlannedCourseController;
import nl.han.oose.iecdapplication.enums.Status;
import nl.han.oose.iecdapplication.models.PlannedCourse;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class PlannedCourseEntityModelAssembler implements RepresentationModelAssembler<PlannedCourse, EntityModel<PlannedCourse>> {

    @Override
    public EntityModel<PlannedCourse> toModel(PlannedCourse plannedCourse) {

        EntityModel<PlannedCourse> plannedCourseEntityModel = new EntityModel<>(plannedCourse);

        if (plannedCourse.getStatus().equals(Status.ACTIVE)) {
            plannedCourseEntityModel.add(
                    linkTo(methodOn(PlannedCourseController.class).deactivate(plannedCourse.getId())).withRel("deactivate"));
        } else {
            plannedCourseEntityModel.add(
                    linkTo(methodOn(PlannedCourseController.class).activate(plannedCourse.getId())).withRel("activate"));
        }

        return plannedCourseEntityModel;
    }
}
