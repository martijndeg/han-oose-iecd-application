package nl.han.oose.iecdapplication.security.services;

import lombok.SneakyThrows;
import nl.han.oose.iecdapplication.models.Teacher;
import nl.han.oose.iecdapplication.repositories.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    TeacherRepository teacherRepository;

    @SneakyThrows
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Teacher teacher = teacherRepository.findByEmail(username);

        return UserDetailsImpl.build(teacher);
    }
}
