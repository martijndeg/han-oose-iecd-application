package nl.han.oose.iecdapplication.models;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
public class Class {

    private @Id
    @GeneratedValue
    Long id;
    private String name;

    public Class() {
    }

    public Class(String name) {
        this.name = name;
    }

    @ManyToMany(mappedBy = "classes")
    Set<Course> courses = new HashSet<>();

    @ManyToMany(targetEntity = Student.class, fetch = FetchType.EAGER)
    Set<Student> students = new HashSet<>();

    @ManyToOne(cascade = {CascadeType.ALL})
    Teacher createdBy;

    @OneToMany
    Set<Lesson> lessons = new HashSet<>();

    @ManyToOne
    SchoolYear schoolYear;
}
