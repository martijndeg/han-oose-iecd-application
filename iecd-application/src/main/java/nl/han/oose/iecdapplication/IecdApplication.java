package nl.han.oose.iecdapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IecdApplication {

    public static void main(String[] args) {
        SpringApplication.run(IecdApplication.class, args);
    }

}
